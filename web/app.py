from flask import Flask, request
from waitress import serve

app = Flask(__name__, static_url_path='')


@app.route('/')
def root():
    return app.send_static_file('index.html')


serve(app, host='0.0.0.0', port=8080)
