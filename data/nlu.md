## intent:greet
- oi
- eae
- eai
- e aí
- olá
- bom dia
- boa tarde
- boa noite
- ea i
- hello
- Oi
- olá brother
- Dale
- Opa

## intent:help
- qual a sua funcionalidade?
- o que você pode fazer?
- qual sua função?
- preciso de ajuda
- oq vc faz
- oque tu pode fazer ?
- qual sua funcionalidade
- menu
- quero ir pro menu
- help
- me ajuda

## intent:game_players
- Quantas pessoas estão jogando [Skyrim](game)?
- Quantos players estão jogando [Skyrim](game)?
- Quantas pessoas estão jogando [The Witcher 3](game)?
- Quantos players estão jogando [The Witcher 3](game)?
- Quantas pessoas estão jogando?
- Quantos players estão jogando?
- Quero saber quantas pessoas estão jogando [Skyrim](game)
- Quero saber quantos players estão jogando [Skyrim](game)
- Quero saber quantas pessoas estão jogando [The Witcher 3](game)
- Quero saber quantos players estão jogando [The Witcher 3](game)
- Quero saber quantas pessoas estão jogando
- Quero saber quantos players estão jogando
- Quantia de jogadores
- Quantia
- Quantidade de jogadores
- Quantidade de jogadores de [The Witcher 3](game)
- jogadores [Conan Exiles](game)

## intent:game_price
- Qual o preço atual do jogo [Skyrim](game)?
- Qual o preço de [Skyrim](game)?
- Qual o preço de [Dota 2](game)?
- Quanto está [Skyrim](game)?
- Quanto está custando [Skyrim](game)?
- Gostaria de saber o preço de um jogo
- [Skyrim](game) está em promoção?
- gostaria de saber o preço de [Factorio](game)
- gostaria de saber o preço de [Skyrim](game)
- Qual o preço de [Fall Guys](game)?
- quanto está custando [Counter Strike](game)?
- Quanto custa [Skyrim](game)?
- Gostaria de saber o preço de [Age of Empires](game)
- Gostaria de saber o preço de [Skyrim](game)
- preço atual de um jogo
- quanto custa [the sims](game)?
- cara quero um jogo
- valor do [monster hunter](game)
- to pensando em pesquisar algum game
- pesquisa game
- jogo [the witcher](game)
- e o [the witcher](game), quanto q ta?
- preço do [the witcher](game)

## intent:goodbye
- tchau
- até mais
- até logo
- flw
- falou
- vou sair
- sair
- Exit
- adeus
- não quero fazer nenhuma busca

## intent:out_of_scope
- sextou
- Toca uma musica
- hahaha
- ..
- Onde você está
- O que está fazendo
- Estou com sono
- Quero falar com alguém
- Quero falar com o zelda
- quero falar com o Mario man
- gostaria de falar com o Mário
- vai chover hoje?
- previsão do tempo para a semana
- previsão do tempo

## intent:bot_challenge
- Quem é você?
- Não sei quem você é
- quem é vc
- qual seu nome?

## intent:none
Nenhum dos anteriores
Nenhum desses
Não listado
Não está entre as opções
Nenhum
Não está na lista
Não esta listado
Não é nenhum desses
Não tá na lista
Nada