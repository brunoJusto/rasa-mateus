## start
* start
    - utter_who_am_i
    - action_help

## greet
* greet
  - action_greet
  - action_help

## goodbye
* goodbye
  - utter_goodbye

## start-goodbye
* start
    - utter_who_am_i
    - action_help
* goodbye
  - utter_goodbye

## bot_challenge
* bot_challenge
  - utter_who_am_i

## help
* help
  - action_help

## out_of_scope
* out_of_scope
  - action_fallback

## oi tchau
* greet
    - action_greet
    - action_help
* goodbye
    - utter_goodbye
    
    

## game_price 1 resultado
* game_price
    - utter_ask_game_name
* game{"game": "Monster Hunter"}
    - slot{"game": "Monster Hunter"}
    - action_game_search
    - slot{"game_appid": 582010}
    - action_game_price
    - slot{"game": null}
    - slot{"game_index": 0}
    - slot{"found_games": null}
    - slot{"game_appid": null}
    - utter_anything_else

## game_price varios resultados escolha direta
* game_price
    - utter_ask_game_name
* game{"game": "age of empires"}
    - slot{"game": "age of empires"}
    - action_game_search
    - slot{"game_index": 5}
    - slot{"found_games": [{"appid": 105450, "name": "Age of Empires® III (2007)", "last_modified": 1598658019, "price_change_number": 9312454}, {"appid": 221380, "name": "Age of Empires II (2013)", "last_modified": 1567618315, "price_change_number": 6804720}, {"appid": 813780, "name": "Age of Empires II: Definitive Edition", "last_modified": 1598909139, "price_change_number": 9270555}, {"appid": 933110, "name": "Age of Empires III: Definitive Edition", "last_modified": 1598644372, "price_change_number": 9301696}, {"appid": 1017900, "name": "Age of Empires: Definitive Edition", "last_modified": 1596740984, "price_change_number": 9020750}]}
* game_appid{"game_appid": 933110}
    - slot{"game_appid": 933110}
    - action_game_price
    - slot{"game": null}
    - slot{"game_index": 0}
    - slot{"found_games": null}
    - slot{"game_appid": null}
    - utter_anything_else

## game_price varios resultados none então escolha
* game_price
    - utter_ask_game_name
* game{"game": "FINAL FANTASY"}
    - slot{"game": "FINAL FANTASY"}
    - action_game_search
    - slot{"game_index": 5}
    - slot{"found_games": [{"appid": 39140, "name": "FINAL FANTASY VII", "last_modified": 1574952550, "price_change_number": 9550283}, {"appid": 39150, "name": "FINAL FANTASY VIII", "last_modified": 1580830590, "price_change_number": 9550283}, {"appid": 39210, "name": "FINAL FANTASY XIV Online", "last_modified": 1600280372, "price_change_number": 6949881}, {"appid": 230330, "name": "FINAL FANTASY® XI: Ultimate Collection Seekers Edition", "last_modified": 1531736067, "price_change_number": 9331095}, {"appid": 230350, "name": "FINAL FANTASY®  XI: Ultimate Collection Seekers Edition", "last_modified": 1549382177, "price_change_number": 9331095}, {"appid": 239120, "name": "FINAL FANTASY III", "last_modified": 1585304945, "price_change_number": 9550283}, {"appid": 292120, "name": "FINAL FANTASY® XIII", "last_modified": 1580830392, "price_change_number": 9550283}, {"appid": 292140, "name": "FINAL FANTASY® XIII-2", "last_modified": 1573466446, "price_change_number": 9550283}, {"appid": 312750, "name": "FINAL FANTASY IV", "last_modified": 1578415275, "price_change_number": 9550283}, {"appid": 340170, "name": "FINAL FANTASY TYPE-0™ HD", "last_modified": 1580830865, "price_change_number": 9550283}, {"appid": 345350, "name": "LIGHTNING RETURNS™: FINAL FANTASY® XIII", "last_modified": 1572250705, "price_change_number": 9550283}, {"appid": 346830, "name": "FINAL FANTASY IV: THE AFTER YEARS", "last_modified": 1547739205, "price_change_number": 9550283}, {"appid": 359870, "name": "FINAL FANTASY X/X-2 HD Remaster", "last_modified": 1567793822, "price_change_number": 9550283}, {"appid": 377840, "name": "FINAL FANTASY IX", "last_modified": 1596707344, "price_change_number": 9550283}, {"appid": 382890, "name": "FINAL FANTASY V", "last_modified": 1578415532, "price_change_number": 9550283}, {"appid": 382900, "name": "FINAL FANTASY VI", "last_modified": 1578415704, "price_change_number": 9550283}, {"appid": 552700, "name": "WORLD OF FINAL FANTASY®", "last_modified": 1580830916, "price_change_number": 9550283}, {"appid": 595520, "name": "FINAL FANTASY XII THE ZODIAC AGE", "last_modified": 1588783777, "price_change_number": 9550283}, {"appid": 637650, "name": "FINAL FANTASY XV WINDOWS EDITION", "last_modified": 1592962568, "price_change_number": 9550283}, {"appid": 648600, "name": "FINAL FANTASY XV WINDOWS EDITION MOD ORGANIZER", "last_modified": 1538145960, "price_change_number": 0}, {"appid": 921590, "name": "DISSIDIA FINAL FANTASY NT Free Edition", "last_modified": 1583341768, "price_change_number": 9550283}, {"appid": 1026680, "name": "FINAL FANTASY VIII - REMASTERED", "last_modified": 1582570782, "price_change_number": 9550283}]}
* none
    - action_game_search
    - slot{"game_index": 10}
* game_appid{"game_appid": 292120}
    - slot{"game_appid": 292120}
    - action_game_price
    - slot{"game": null}
    - slot{"game_index": 0}
    - slot{"found_games": null}
    - slot{"game_appid": null}
    - utter_anything_else

## game_price varios resultados none até acabar
* game_price
    - utter_ask_game_name
* game{"game": "FINAL FANTASY"}
    - slot{"game": "FINAL FANTASY"}
    - action_game_search
    - slot{"game_index": 5}
    - slot{"found_games": [{"appid": 39140, "name": "FINAL FANTASY VII", "last_modified": 1574952550, "price_change_number": 9550283}, {"appid": 39150, "name": "FINAL FANTASY VIII", "last_modified": 1580830590, "price_change_number": 9550283}, {"appid": 39210, "name": "FINAL FANTASY XIV Online", "last_modified": 1600280372, "price_change_number": 6949881}, {"appid": 230330, "name": "FINAL FANTASY® XI: Ultimate Collection Seekers Edition", "last_modified": 1531736067, "price_change_number": 9331095}, {"appid": 230350, "name": "FINAL FANTASY®  XI: Ultimate Collection Seekers Edition", "last_modified": 1549382177, "price_change_number": 9331095}, {"appid": 239120, "name": "FINAL FANTASY III", "last_modified": 1585304945, "price_change_number": 9550283}, {"appid": 292120, "name": "FINAL FANTASY® XIII", "last_modified": 1580830392, "price_change_number": 9550283}, {"appid": 292140, "name": "FINAL FANTASY® XIII-2", "last_modified": 1573466446, "price_change_number": 9550283}, {"appid": 312750, "name": "FINAL FANTASY IV", "last_modified": 1578415275, "price_change_number": 9550283}, {"appid": 340170, "name": "FINAL FANTASY TYPE-0™ HD", "last_modified": 1580830865, "price_change_number": 9550283}, {"appid": 345350, "name": "LIGHTNING RETURNS™: FINAL FANTASY® XIII", "last_modified": 1572250705, "price_change_number": 9550283}, {"appid": 346830, "name": "FINAL FANTASY IV: THE AFTER YEARS", "last_modified": 1547739205, "price_change_number": 9550283}, {"appid": 359870, "name": "FINAL FANTASY X/X-2 HD Remaster", "last_modified": 1567793822, "price_change_number": 9550283}, {"appid": 377840, "name": "FINAL FANTASY IX", "last_modified": 1596707344, "price_change_number": 9550283}, {"appid": 382890, "name": "FINAL FANTASY V", "last_modified": 1578415532, "price_change_number": 9550283}, {"appid": 382900, "name": "FINAL FANTASY VI", "last_modified": 1578415704, "price_change_number": 9550283}, {"appid": 552700, "name": "WORLD OF FINAL FANTASY®", "last_modified": 1580830916, "price_change_number": 9550283}, {"appid": 595520, "name": "FINAL FANTASY XII THE ZODIAC AGE", "last_modified": 1588783777, "price_change_number": 9550283}, {"appid": 637650, "name": "FINAL FANTASY XV WINDOWS EDITION", "last_modified": 1592962568, "price_change_number": 9550283}, {"appid": 648600, "name": "FINAL FANTASY XV WINDOWS EDITION MOD ORGANIZER", "last_modified": 1538145960, "price_change_number": 0}, {"appid": 921590, "name": "DISSIDIA FINAL FANTASY NT Free Edition", "last_modified": 1583341768, "price_change_number": 9550283}, {"appid": 1026680, "name": "FINAL FANTASY VIII - REMASTERED", "last_modified": 1582570782, "price_change_number": 9550283}]}
* none
    - action_game_search
    - slot{"game_index": 10}
* none
    - action_game_search
    - slot{"game_index": 15}
* none
    - action_game_search
    - slot{"game_index": 20}
* none
    - action_game_search
    - slot{"game_index": 25}
* none
    - action_game_search
    - slot{"game": null}
    - slot{"game_index": 0}
    - slot{"found_games": null}
    - slot{"game_appid": null}
    - utter_anything_else

## game_players 1 resultado
* game_players
    - utter_ask_game_name
* game{"game": "Monster Hunter"}
    - slot{"game": "Monster Hunter"}
    - action_game_search
    - slot{"game_appid": 582010}
    - action_game_players
    - slot{"game": null}
    - slot{"game_index": 0}
    - slot{"found_games": null}
    - slot{"game_appid": null}
    - utter_anything_else

## game_players varios resultados escolha direta
* game_players
    - utter_ask_game_name
* game{"game": "age of empires"}
    - slot{"game": "age of empires"}
    - action_game_search
    - slot{"game_index": 5}
    - slot{"found_games": [{"appid": 105450, "name": "Age of Empires® III (2007)", "last_modified": 1598658019, "price_change_number": 9312454}, {"appid": 221380, "name": "Age of Empires II (2013)", "last_modified": 1567618315, "price_change_number": 6804720}, {"appid": 813780, "name": "Age of Empires II: Definitive Edition", "last_modified": 1598909139, "price_change_number": 9270555}, {"appid": 933110, "name": "Age of Empires III: Definitive Edition", "last_modified": 1598644372, "price_change_number": 9301696}, {"appid": 1017900, "name": "Age of Empires: Definitive Edition", "last_modified": 1596740984, "price_change_number": 9020750}]}
* game_appid{"game_appid": 933110}
    - slot{"game_appid": 933110}
    - action_game_players
    - slot{"game": null}
    - slot{"game_index": 0}
    - slot{"found_games": null}
    - slot{"game_appid": null}
    - utter_anything_else

## game_players varios resultados none então escolha
* game_players
    - utter_ask_game_name
* game{"game": "FINAL FANTASY"}
    - slot{"game": "FINAL FANTASY"}
    - action_game_search
    - slot{"game_index": 5}
    - slot{"found_games": [{"appid": 39140, "name": "FINAL FANTASY VII", "last_modified": 1574952550, "price_change_number": 9550283}, {"appid": 39150, "name": "FINAL FANTASY VIII", "last_modified": 1580830590, "price_change_number": 9550283}, {"appid": 39210, "name": "FINAL FANTASY XIV Online", "last_modified": 1600280372, "price_change_number": 6949881}, {"appid": 230330, "name": "FINAL FANTASY® XI: Ultimate Collection Seekers Edition", "last_modified": 1531736067, "price_change_number": 9331095}, {"appid": 230350, "name": "FINAL FANTASY®  XI: Ultimate Collection Seekers Edition", "last_modified": 1549382177, "price_change_number": 9331095}, {"appid": 239120, "name": "FINAL FANTASY III", "last_modified": 1585304945, "price_change_number": 9550283}, {"appid": 292120, "name": "FINAL FANTASY® XIII", "last_modified": 1580830392, "price_change_number": 9550283}, {"appid": 292140, "name": "FINAL FANTASY® XIII-2", "last_modified": 1573466446, "price_change_number": 9550283}, {"appid": 312750, "name": "FINAL FANTASY IV", "last_modified": 1578415275, "price_change_number": 9550283}, {"appid": 340170, "name": "FINAL FANTASY TYPE-0™ HD", "last_modified": 1580830865, "price_change_number": 9550283}, {"appid": 345350, "name": "LIGHTNING RETURNS™: FINAL FANTASY® XIII", "last_modified": 1572250705, "price_change_number": 9550283}, {"appid": 346830, "name": "FINAL FANTASY IV: THE AFTER YEARS", "last_modified": 1547739205, "price_change_number": 9550283}, {"appid": 359870, "name": "FINAL FANTASY X/X-2 HD Remaster", "last_modified": 1567793822, "price_change_number": 9550283}, {"appid": 377840, "name": "FINAL FANTASY IX", "last_modified": 1596707344, "price_change_number": 9550283}, {"appid": 382890, "name": "FINAL FANTASY V", "last_modified": 1578415532, "price_change_number": 9550283}, {"appid": 382900, "name": "FINAL FANTASY VI", "last_modified": 1578415704, "price_change_number": 9550283}, {"appid": 552700, "name": "WORLD OF FINAL FANTASY®", "last_modified": 1580830916, "price_change_number": 9550283}, {"appid": 595520, "name": "FINAL FANTASY XII THE ZODIAC AGE", "last_modified": 1588783777, "price_change_number": 9550283}, {"appid": 637650, "name": "FINAL FANTASY XV WINDOWS EDITION", "last_modified": 1592962568, "price_change_number": 9550283}, {"appid": 648600, "name": "FINAL FANTASY XV WINDOWS EDITION MOD ORGANIZER", "last_modified": 1538145960, "price_change_number": 0}, {"appid": 921590, "name": "DISSIDIA FINAL FANTASY NT Free Edition", "last_modified": 1583341768, "price_change_number": 9550283}, {"appid": 1026680, "name": "FINAL FANTASY VIII - REMASTERED", "last_modified": 1582570782, "price_change_number": 9550283}]}
* none
    - action_game_search
    - slot{"game_index": 10}
* game_appid{"game_appid": 292120}
    - slot{"game_appid": 292120}
    - action_game_players
    - slot{"game": null}
    - slot{"game_index": 0}
    - slot{"found_games": null}
    - slot{"game_appid": null}
    - utter_anything_else

## game_players varios resultados none até acabar
* game_players
    - utter_ask_game_name
* game{"game": "FINAL FANTASY"}
    - slot{"game": "FINAL FANTASY"}
    - action_game_search
    - slot{"game_index": 5}
    - slot{"found_games": [{"appid": 39140, "name": "FINAL FANTASY VII", "last_modified": 1574952550, "price_change_number": 9550283}, {"appid": 39150, "name": "FINAL FANTASY VIII", "last_modified": 1580830590, "price_change_number": 9550283}, {"appid": 39210, "name": "FINAL FANTASY XIV Online", "last_modified": 1600280372, "price_change_number": 6949881}, {"appid": 230330, "name": "FINAL FANTASY® XI: Ultimate Collection Seekers Edition", "last_modified": 1531736067, "price_change_number": 9331095}, {"appid": 230350, "name": "FINAL FANTASY®  XI: Ultimate Collection Seekers Edition", "last_modified": 1549382177, "price_change_number": 9331095}, {"appid": 239120, "name": "FINAL FANTASY III", "last_modified": 1585304945, "price_change_number": 9550283}, {"appid": 292120, "name": "FINAL FANTASY® XIII", "last_modified": 1580830392, "price_change_number": 9550283}, {"appid": 292140, "name": "FINAL FANTASY® XIII-2", "last_modified": 1573466446, "price_change_number": 9550283}, {"appid": 312750, "name": "FINAL FANTASY IV", "last_modified": 1578415275, "price_change_number": 9550283}, {"appid": 340170, "name": "FINAL FANTASY TYPE-0™ HD", "last_modified": 1580830865, "price_change_number": 9550283}, {"appid": 345350, "name": "LIGHTNING RETURNS™: FINAL FANTASY® XIII", "last_modified": 1572250705, "price_change_number": 9550283}, {"appid": 346830, "name": "FINAL FANTASY IV: THE AFTER YEARS", "last_modified": 1547739205, "price_change_number": 9550283}, {"appid": 359870, "name": "FINAL FANTASY X/X-2 HD Remaster", "last_modified": 1567793822, "price_change_number": 9550283}, {"appid": 377840, "name": "FINAL FANTASY IX", "last_modified": 1596707344, "price_change_number": 9550283}, {"appid": 382890, "name": "FINAL FANTASY V", "last_modified": 1578415532, "price_change_number": 9550283}, {"appid": 382900, "name": "FINAL FANTASY VI", "last_modified": 1578415704, "price_change_number": 9550283}, {"appid": 552700, "name": "WORLD OF FINAL FANTASY®", "last_modified": 1580830916, "price_change_number": 9550283}, {"appid": 595520, "name": "FINAL FANTASY XII THE ZODIAC AGE", "last_modified": 1588783777, "price_change_number": 9550283}, {"appid": 637650, "name": "FINAL FANTASY XV WINDOWS EDITION", "last_modified": 1592962568, "price_change_number": 9550283}, {"appid": 648600, "name": "FINAL FANTASY XV WINDOWS EDITION MOD ORGANIZER", "last_modified": 1538145960, "price_change_number": 0}, {"appid": 921590, "name": "DISSIDIA FINAL FANTASY NT Free Edition", "last_modified": 1583341768, "price_change_number": 9550283}, {"appid": 1026680, "name": "FINAL FANTASY VIII - REMASTERED", "last_modified": 1582570782, "price_change_number": 9550283}]}
* none
    - action_game_search
    - slot{"game_index": 10}
* none
    - action_game_search
    - slot{"game_index": 15}
* none
    - action_game_search
    - slot{"game_index": 20}
* none
    - action_game_search
    - slot{"game_index": 25}
* none
    - action_game_search
    - slot{"game": null}
    - slot{"game_index": 0}
    - slot{"found_games": null}
    - slot{"game_appid": null}
    - utter_anything_else

## game_players to goodbye
* game_players
    - utter_ask_game_name
* game{"game": "FINAL FANTASY"}
    - slot{"game": "FINAL FANTASY"}
    - action_game_search
    - slot{"game_index": 5}
    - slot{"found_games": [{"appid": 39140, "name": "FINAL FANTASY VII", "last_modified": 1574952550, "price_change_number": 9550283}, {"appid": 39150, "name": "FINAL FANTASY VIII", "last_modified": 1580830590, "price_change_number": 9550283}, {"appid": 39210, "name": "FINAL FANTASY XIV Online", "last_modified": 1600280372, "price_change_number": 6949881}, {"appid": 230330, "name": "FINAL FANTASY® XI: Ultimate Collection Seekers Edition", "last_modified": 1531736067, "price_change_number": 9331095}, {"appid": 230350, "name": "FINAL FANTASY®  XI: Ultimate Collection Seekers Edition", "last_modified": 1549382177, "price_change_number": 9331095}, {"appid": 239120, "name": "FINAL FANTASY III", "last_modified": 1585304945, "price_change_number": 9550283}, {"appid": 292120, "name": "FINAL FANTASY® XIII", "last_modified": 1580830392, "price_change_number": 9550283}, {"appid": 292140, "name": "FINAL FANTASY® XIII-2", "last_modified": 1573466446, "price_change_number": 9550283}, {"appid": 312750, "name": "FINAL FANTASY IV", "last_modified": 1578415275, "price_change_number": 9550283}, {"appid": 340170, "name": "FINAL FANTASY TYPE-0™ HD", "last_modified": 1580830865, "price_change_number": 9550283}, {"appid": 345350, "name": "LIGHTNING RETURNS™: FINAL FANTASY® XIII", "last_modified": 1572250705, "price_change_number": 9550283}, {"appid": 346830, "name": "FINAL FANTASY IV: THE AFTER YEARS", "last_modified": 1547739205, "price_change_number": 9550283}, {"appid": 359870, "name": "FINAL FANTASY X/X-2 HD Remaster", "last_modified": 1567793822, "price_change_number": 9550283}, {"appid": 377840, "name": "FINAL FANTASY IX", "last_modified": 1596707344, "price_change_number": 9550283}, {"appid": 382890, "name": "FINAL FANTASY V", "last_modified": 1578415532, "price_change_number": 9550283}, {"appid": 382900, "name": "FINAL FANTASY VI", "last_modified": 1578415704, "price_change_number": 9550283}, {"appid": 552700, "name": "WORLD OF FINAL FANTASY®", "last_modified": 1580830916, "price_change_number": 9550283}, {"appid": 595520, "name": "FINAL FANTASY XII THE ZODIAC AGE", "last_modified": 1588783777, "price_change_number": 9550283}, {"appid": 637650, "name": "FINAL FANTASY XV WINDOWS EDITION", "last_modified": 1592962568, "price_change_number": 9550283}, {"appid": 648600, "name": "FINAL FANTASY XV WINDOWS EDITION MOD ORGANIZER", "last_modified": 1538145960, "price_change_number": 0}, {"appid": 921590, "name": "DISSIDIA FINAL FANTASY NT Free Edition", "last_modified": 1583341768, "price_change_number": 9550283}, {"appid": 1026680, "name": "FINAL FANTASY VIII - REMASTERED", "last_modified": 1582570782, "price_change_number": 9550283}]}
* goodbye
    - utter_goodbye
    - action_reset_entities
    - slot{"game": null}
    - slot{"game_index": 0}
    - slot{"found_games": null}
    - slot{"game_appid": null}
    

## game_price to goodbye
* game_price
    - utter_ask_game_name
* game{"game": "FINAL FANTASY"}
    - slot{"game": "FINAL FANTASY"}
    - action_game_search
    - slot{"game_index": 5}
    - slot{"found_games": [{"appid": 39140, "name": "FINAL FANTASY VII", "last_modified": 1574952550, "price_change_number": 9550283}, {"appid": 39150, "name": "FINAL FANTASY VIII", "last_modified": 1580830590, "price_change_number": 9550283}, {"appid": 39210, "name": "FINAL FANTASY XIV Online", "last_modified": 1600280372, "price_change_number": 6949881}, {"appid": 230330, "name": "FINAL FANTASY® XI: Ultimate Collection Seekers Edition", "last_modified": 1531736067, "price_change_number": 9331095}, {"appid": 230350, "name": "FINAL FANTASY®  XI: Ultimate Collection Seekers Edition", "last_modified": 1549382177, "price_change_number": 9331095}, {"appid": 239120, "name": "FINAL FANTASY III", "last_modified": 1585304945, "price_change_number": 9550283}, {"appid": 292120, "name": "FINAL FANTASY® XIII", "last_modified": 1580830392, "price_change_number": 9550283}, {"appid": 292140, "name": "FINAL FANTASY® XIII-2", "last_modified": 1573466446, "price_change_number": 9550283}, {"appid": 312750, "name": "FINAL FANTASY IV", "last_modified": 1578415275, "price_change_number": 9550283}, {"appid": 340170, "name": "FINAL FANTASY TYPE-0™ HD", "last_modified": 1580830865, "price_change_number": 9550283}, {"appid": 345350, "name": "LIGHTNING RETURNS™: FINAL FANTASY® XIII", "last_modified": 1572250705, "price_change_number": 9550283}, {"appid": 346830, "name": "FINAL FANTASY IV: THE AFTER YEARS", "last_modified": 1547739205, "price_change_number": 9550283}, {"appid": 359870, "name": "FINAL FANTASY X/X-2 HD Remaster", "last_modified": 1567793822, "price_change_number": 9550283}, {"appid": 377840, "name": "FINAL FANTASY IX", "last_modified": 1596707344, "price_change_number": 9550283}, {"appid": 382890, "name": "FINAL FANTASY V", "last_modified": 1578415532, "price_change_number": 9550283}, {"appid": 382900, "name": "FINAL FANTASY VI", "last_modified": 1578415704, "price_change_number": 9550283}, {"appid": 552700, "name": "WORLD OF FINAL FANTASY®", "last_modified": 1580830916, "price_change_number": 9550283}, {"appid": 595520, "name": "FINAL FANTASY XII THE ZODIAC AGE", "last_modified": 1588783777, "price_change_number": 9550283}, {"appid": 637650, "name": "FINAL FANTASY XV WINDOWS EDITION", "last_modified": 1592962568, "price_change_number": 9550283}, {"appid": 648600, "name": "FINAL FANTASY XV WINDOWS EDITION MOD ORGANIZER", "last_modified": 1538145960, "price_change_number": 0}, {"appid": 921590, "name": "DISSIDIA FINAL FANTASY NT Free Edition", "last_modified": 1583341768, "price_change_number": 9550283}, {"appid": 1026680, "name": "FINAL FANTASY VIII - REMASTERED", "last_modified": 1582570782, "price_change_number": 9550283}]}
* goodbye
    - utter_goodbye
    - action_reset_entities
    - slot{"game": null}
    - slot{"game_index": 0}
    - slot{"found_games": null}
    - slot{"game_appid": null}
    
## game_price to game_players 1 resultado
* game_price
    - utter_ask_game_name
* game{"game": "FINAL FANTASY"}
    - slot{"game": "FINAL FANTASY"}
    - action_game_search
    - slot{"game_index": 5}
    - slot{"found_games": [{"appid": 39140, "name": "FINAL FANTASY VII", "last_modified": 1574952550, "price_change_number": 9550283}, {"appid": 39150, "name": "FINAL FANTASY VIII", "last_modified": 1580830590, "price_change_number": 9550283}, {"appid": 39210, "name": "FINAL FANTASY XIV Online", "last_modified": 1600280372, "price_change_number": 6949881}, {"appid": 230330, "name": "FINAL FANTASY® XI: Ultimate Collection Seekers Edition", "last_modified": 1531736067, "price_change_number": 9331095}, {"appid": 230350, "name": "FINAL FANTASY®  XI: Ultimate Collection Seekers Edition", "last_modified": 1549382177, "price_change_number": 9331095}, {"appid": 239120, "name": "FINAL FANTASY III", "last_modified": 1585304945, "price_change_number": 9550283}, {"appid": 292120, "name": "FINAL FANTASY® XIII", "last_modified": 1580830392, "price_change_number": 9550283}, {"appid": 292140, "name": "FINAL FANTASY® XIII-2", "last_modified": 1573466446, "price_change_number": 9550283}, {"appid": 312750, "name": "FINAL FANTASY IV", "last_modified": 1578415275, "price_change_number": 9550283}, {"appid": 340170, "name": "FINAL FANTASY TYPE-0™ HD", "last_modified": 1580830865, "price_change_number": 9550283}, {"appid": 345350, "name": "LIGHTNING RETURNS™: FINAL FANTASY® XIII", "last_modified": 1572250705, "price_change_number": 9550283}, {"appid": 346830, "name": "FINAL FANTASY IV: THE AFTER YEARS", "last_modified": 1547739205, "price_change_number": 9550283}, {"appid": 359870, "name": "FINAL FANTASY X/X-2 HD Remaster", "last_modified": 1567793822, "price_change_number": 9550283}, {"appid": 377840, "name": "FINAL FANTASY IX", "last_modified": 1596707344, "price_change_number": 9550283}, {"appid": 382890, "name": "FINAL FANTASY V", "last_modified": 1578415532, "price_change_number": 9550283}, {"appid": 382900, "name": "FINAL FANTASY VI", "last_modified": 1578415704, "price_change_number": 9550283}, {"appid": 552700, "name": "WORLD OF FINAL FANTASY®", "last_modified": 1580830916, "price_change_number": 9550283}, {"appid": 595520, "name": "FINAL FANTASY XII THE ZODIAC AGE", "last_modified": 1588783777, "price_change_number": 9550283}, {"appid": 637650, "name": "FINAL FANTASY XV WINDOWS EDITION", "last_modified": 1592962568, "price_change_number": 9550283}, {"appid": 648600, "name": "FINAL FANTASY XV WINDOWS EDITION MOD ORGANIZER", "last_modified": 1538145960, "price_change_number": 0}, {"appid": 921590, "name": "DISSIDIA FINAL FANTASY NT Free Edition", "last_modified": 1583341768, "price_change_number": 9550283}, {"appid": 1026680, "name": "FINAL FANTASY VIII - REMASTERED", "last_modified": 1582570782, "price_change_number": 9550283}]}
* game_players{"game": "Dota 2"}
    - slot{"game": "Dota 2"}
    - action_reset_entities
    - slot{"game": null}
    - slot{"game_index": 0}
    - slot{"found_games": null}
    - slot{"game_appid": null}
    - action_game_search
    - slot{"game_appid": 570}
    - action_game_players
    - slot{"game": null}
    - slot{"game_index": 0}
    - slot{"found_games": null}
    - slot{"game_appid": null}
    - utter_anything_else

## game_price to game_players varios resultados none então escolha
* game_price
    - utter_ask_game_name
* game{"game": "cyberpunk"}
    - slot{"game": "cyberpunk"}
    - action_game_search
    - slot{"game_index": 5}
    - slot{"found_games": [{"appid": 343170, "name": "Cyberpunk 3776", "last_modified": 1573691977, "price_change_number": 7885946}, {"appid": 447530, "name": "VA-11 Hall-A: Cyberpunk Bartender Action", "last_modified": 1568782899, "price_change_number": 8896429}, {"appid": 555340, "name": "Bitlogic - A Cyberpunk Arcade Adventure", "last_modified": 1572509982, "price_change_number": 7469765}, {"appid": 614970, "name": "Qbike: Cyberpunk Motorcycles", "last_modified": 1567096850, "price_change_number": 9517843}, {"appid": 742470, "name": "Chronicles of cyberpunk", "last_modified": 1600954501, "price_change_number": 0}, {"appid": 822990, "name": "Spinnortality | cyberpunk management sim", "last_modified": 1596797704, "price_change_number": 8896429}, {"appid": 914210, "name": "N1RV Ann-A: Cyberpunk Bartender Action", "last_modified": 1587661847, "price_change_number": 0}, {"appid": 928000, "name": "Solace State: Emotional Cyberpunk Stories", "last_modified": 1600563611, "price_change_number": 0}, {"appid": 990900, "name": "Neon Noodles - Cyberpunk Kitchen Automation", "last_modified": 1585242739, "price_change_number": 8896429}, {"appid": 1091500, "name": "Cyberpunk 2077", "last_modified": 1600446313, "price_change_number": 6955096}, {"appid": 1116570, "name": "Electric Sheep: A Cyberpunk Dystopia", "last_modified": 1598305162, "price_change_number": 0}, {"appid": 1120560, "name": "Sense - 不祥的预感: A Cyberpunk Ghost Story", "last_modified": 1599152709, "price_change_number": 9344343}, {"appid": 1125760, "name": "Cyberpunk Girl", "last_modified": 1572859658, "price_change_number": 9565033}, {"appid": 1134180, "name": "Cyberpunk hentai: Memory leak", "last_modified": 1587925874, "price_change_number": 9399857}, {"appid": 1146070, "name": "Terminal Protocol: Cyberpunk Turn-Based Tactics", "last_modified": 1595964288, "price_change_number": 0}, {"appid": 1175950, "name": "Cyberpunk Sex Simulator", "last_modified": 1572908833, "price_change_number": 9637059}, {"appid": 1188850, "name": "Cyberpunk Bar Sim", "last_modified": 1601929477, "price_change_number": 0}, {"appid": 1252170, "name": "InfiniteCorp: Cyberpunk Revolution", "last_modified": 1600264746, "price_change_number": 8288425}, {"appid": 1324400, "name": "Cyberpunk Detective", "last_modified": 1600861998, "price_change_number": 0}, {"appid": 1325040, "name": "Keylocker | Turn Based Cyberpunk Action", "last_modified": 1601695901, "price_change_number": 0}, {"appid": 1361680, "name": "cyberpunkdreams", "last_modified": 1596186478, "price_change_number": 0}, {"appid": 1372090, "name": "Hentai Cyberpunk", "last_modified": 1597333802, "price_change_number": 9175094}]}
* game_players{"game": "Final fantasy"}
    - slot{"game": "Final fantasy"}
    - action_reset_entities
    - slot{"game": null}
    - slot{"game_index": 0}
    - slot{"found_games": null}
    - slot{"game_appid": null}
    - action_game_search
    - slot{"game": "Final fantasy"}
    - slot{"game_index": 5}
    - slot{"found_games": [{"appid": 39140, "name": "FINAL FANTASY VII", "last_modified": 1574952550, "price_change_number": 9550283}, {"appid": 39150, "name": "FINAL FANTASY VIII", "last_modified": 1580830590, "price_change_number": 9550283}, {"appid": 39210, "name": "FINAL FANTASY XIV Online", "last_modified": 1600280372, "price_change_number": 6949881}, {"appid": 230330, "name": "FINAL FANTASY® XI: Ultimate Collection Seekers Edition", "last_modified": 1531736067, "price_change_number": 9331095}, {"appid": 230350, "name": "FINAL FANTASY®  XI: Ultimate Collection Seekers Edition", "last_modified": 1549382177, "price_change_number": 9331095}, {"appid": 239120, "name": "FINAL FANTASY III", "last_modified": 1585304945, "price_change_number": 9550283}, {"appid": 292120, "name": "FINAL FANTASY® XIII", "last_modified": 1580830392, "price_change_number": 9550283}, {"appid": 292140, "name": "FINAL FANTASY® XIII-2", "last_modified": 1573466446, "price_change_number": 9550283}, {"appid": 312750, "name": "FINAL FANTASY IV", "last_modified": 1578415275, "price_change_number": 9550283}, {"appid": 340170, "name": "FINAL FANTASY TYPE-0™ HD", "last_modified": 1580830865, "price_change_number": 9550283}, {"appid": 345350, "name": "LIGHTNING RETURNS™: FINAL FANTASY® XIII", "last_modified": 1572250705, "price_change_number": 9550283}, {"appid": 346830, "name": "FINAL FANTASY IV: THE AFTER YEARS", "last_modified": 1547739205, "price_change_number": 9550283}, {"appid": 359870, "name": "FINAL FANTASY X/X-2 HD Remaster", "last_modified": 1567793822, "price_change_number": 9550283}, {"appid": 377840, "name": "FINAL FANTASY IX", "last_modified": 1596707344, "price_change_number": 9550283}, {"appid": 382890, "name": "FINAL FANTASY V", "last_modified": 1578415532, "price_change_number": 9550283}, {"appid": 382900, "name": "FINAL FANTASY VI", "last_modified": 1578415704, "price_change_number": 9550283}, {"appid": 552700, "name": "WORLD OF FINAL FANTASY®", "last_modified": 1580830916, "price_change_number": 9550283}, {"appid": 595520, "name": "FINAL FANTASY XII THE ZODIAC AGE", "last_modified": 1588783777, "price_change_number": 9550283}, {"appid": 637650, "name": "FINAL FANTASY XV WINDOWS EDITION", "last_modified": 1592962568, "price_change_number": 9550283}, {"appid": 648600, "name": "FINAL FANTASY XV WINDOWS EDITION MOD ORGANIZER", "last_modified": 1538145960, "price_change_number": 0}, {"appid": 921590, "name": "DISSIDIA FINAL FANTASY NT Free Edition", "last_modified": 1583341768, "price_change_number": 9550283}, {"appid": 1026680, "name": "FINAL FANTASY VIII - REMASTERED", "last_modified": 1582570782, "price_change_number": 9550283}]}
* none
    - action_game_search
    - slot{"game_index": 10}
* game_appid{"game_appid": 239120}
    - slot{"game_appid": 239120}
    - action_game_players
    - slot{"game": null}
    - slot{"game_index": 0}
    - slot{"found_games": null}
    - slot{"game_appid": null}
    - utter_anything_else

## game_players to game_price 1 resultado
* game_players
    - utter_ask_game_name
* game{"game": "FINAL FANTASY"}
    - slot{"game": "FINAL FANTASY"}
    - action_game_search
    - slot{"game_index": 5}
    - slot{"found_games": [{"appid": 39140, "name": "FINAL FANTASY VII", "last_modified": 1574952550, "price_change_number": 9550283}, {"appid": 39150, "name": "FINAL FANTASY VIII", "last_modified": 1580830590, "price_change_number": 9550283}, {"appid": 39210, "name": "FINAL FANTASY XIV Online", "last_modified": 1600280372, "price_change_number": 6949881}, {"appid": 230330, "name": "FINAL FANTASY® XI: Ultimate Collection Seekers Edition", "last_modified": 1531736067, "price_change_number": 9331095}, {"appid": 230350, "name": "FINAL FANTASY®  XI: Ultimate Collection Seekers Edition", "last_modified": 1549382177, "price_change_number": 9331095}, {"appid": 239120, "name": "FINAL FANTASY III", "last_modified": 1585304945, "price_change_number": 9550283}, {"appid": 292120, "name": "FINAL FANTASY® XIII", "last_modified": 1580830392, "price_change_number": 9550283}, {"appid": 292140, "name": "FINAL FANTASY® XIII-2", "last_modified": 1573466446, "price_change_number": 9550283}, {"appid": 312750, "name": "FINAL FANTASY IV", "last_modified": 1578415275, "price_change_number": 9550283}, {"appid": 340170, "name": "FINAL FANTASY TYPE-0™ HD", "last_modified": 1580830865, "price_change_number": 9550283}, {"appid": 345350, "name": "LIGHTNING RETURNS™: FINAL FANTASY® XIII", "last_modified": 1572250705, "price_change_number": 9550283}, {"appid": 346830, "name": "FINAL FANTASY IV: THE AFTER YEARS", "last_modified": 1547739205, "price_change_number": 9550283}, {"appid": 359870, "name": "FINAL FANTASY X/X-2 HD Remaster", "last_modified": 1567793822, "price_change_number": 9550283}, {"appid": 377840, "name": "FINAL FANTASY IX", "last_modified": 1596707344, "price_change_number": 9550283}, {"appid": 382890, "name": "FINAL FANTASY V", "last_modified": 1578415532, "price_change_number": 9550283}, {"appid": 382900, "name": "FINAL FANTASY VI", "last_modified": 1578415704, "price_change_number": 9550283}, {"appid": 552700, "name": "WORLD OF FINAL FANTASY®", "last_modified": 1580830916, "price_change_number": 9550283}, {"appid": 595520, "name": "FINAL FANTASY XII THE ZODIAC AGE", "last_modified": 1588783777, "price_change_number": 9550283}, {"appid": 637650, "name": "FINAL FANTASY XV WINDOWS EDITION", "last_modified": 1592962568, "price_change_number": 9550283}, {"appid": 648600, "name": "FINAL FANTASY XV WINDOWS EDITION MOD ORGANIZER", "last_modified": 1538145960, "price_change_number": 0}, {"appid": 921590, "name": "DISSIDIA FINAL FANTASY NT Free Edition", "last_modified": 1583341768, "price_change_number": 9550283}, {"appid": 1026680, "name": "FINAL FANTASY VIII - REMASTERED", "last_modified": 1582570782, "price_change_number": 9550283}]}
* game_price{"game": "Dota 2"}
    - slot{"game": "Dota 2"}
    - action_reset_entities
    - slot{"game": null}
    - slot{"game_index": 0}
    - slot{"found_games": null}
    - slot{"game_appid": null}
    - action_game_search
    - slot{"game_appid": 570}
    - action_game_price
    - slot{"game": null}
    - slot{"game_index": 0}
    - slot{"found_games": null}
    - slot{"game_appid": null}
    - utter_anything_else

## game_players to game_price varios resultados none então escolha
* game_players
    - utter_ask_game_name
* game{"game": "cyberpunk"}
    - slot{"game": "cyberpunk"}
    - action_game_search
    - slot{"game_index": 5}
    - slot{"found_games": [{"appid": 343170, "name": "Cyberpunk 3776", "last_modified": 1573691977, "price_change_number": 7885946}, {"appid": 447530, "name": "VA-11 Hall-A: Cyberpunk Bartender Action", "last_modified": 1568782899, "price_change_number": 8896429}, {"appid": 555340, "name": "Bitlogic - A Cyberpunk Arcade Adventure", "last_modified": 1572509982, "price_change_number": 7469765}, {"appid": 614970, "name": "Qbike: Cyberpunk Motorcycles", "last_modified": 1567096850, "price_change_number": 9517843}, {"appid": 742470, "name": "Chronicles of cyberpunk", "last_modified": 1600954501, "price_change_number": 0}, {"appid": 822990, "name": "Spinnortality | cyberpunk management sim", "last_modified": 1596797704, "price_change_number": 8896429}, {"appid": 914210, "name": "N1RV Ann-A: Cyberpunk Bartender Action", "last_modified": 1587661847, "price_change_number": 0}, {"appid": 928000, "name": "Solace State: Emotional Cyberpunk Stories", "last_modified": 1600563611, "price_change_number": 0}, {"appid": 990900, "name": "Neon Noodles - Cyberpunk Kitchen Automation", "last_modified": 1585242739, "price_change_number": 8896429}, {"appid": 1091500, "name": "Cyberpunk 2077", "last_modified": 1600446313, "price_change_number": 6955096}, {"appid": 1116570, "name": "Electric Sheep: A Cyberpunk Dystopia", "last_modified": 1598305162, "price_change_number": 0}, {"appid": 1120560, "name": "Sense - 不祥的预感: A Cyberpunk Ghost Story", "last_modified": 1599152709, "price_change_number": 9344343}, {"appid": 1125760, "name": "Cyberpunk Girl", "last_modified": 1572859658, "price_change_number": 9565033}, {"appid": 1134180, "name": "Cyberpunk hentai: Memory leak", "last_modified": 1587925874, "price_change_number": 9399857}, {"appid": 1146070, "name": "Terminal Protocol: Cyberpunk Turn-Based Tactics", "last_modified": 1595964288, "price_change_number": 0}, {"appid": 1175950, "name": "Cyberpunk Sex Simulator", "last_modified": 1572908833, "price_change_number": 9637059}, {"appid": 1188850, "name": "Cyberpunk Bar Sim", "last_modified": 1601929477, "price_change_number": 0}, {"appid": 1252170, "name": "InfiniteCorp: Cyberpunk Revolution", "last_modified": 1600264746, "price_change_number": 8288425}, {"appid": 1324400, "name": "Cyberpunk Detective", "last_modified": 1600861998, "price_change_number": 0}, {"appid": 1325040, "name": "Keylocker | Turn Based Cyberpunk Action", "last_modified": 1601695901, "price_change_number": 0}, {"appid": 1361680, "name": "cyberpunkdreams", "last_modified": 1596186478, "price_change_number": 0}, {"appid": 1372090, "name": "Hentai Cyberpunk", "last_modified": 1597333802, "price_change_number": 9175094}]}
* game_price{"game": "Final fantasy"}
    - slot{"game": "Final fantasy"}
    - action_reset_entities
    - slot{"game": null}
    - slot{"game_index": 0}
    - slot{"found_games": null}
    - slot{"game_appid": null}
    - action_game_search
    - slot{"game": "Final fantasy"}
    - slot{"game_index": 5}
    - slot{"found_games": [{"appid": 39140, "name": "FINAL FANTASY VII", "last_modified": 1574952550, "price_change_number": 9550283}, {"appid": 39150, "name": "FINAL FANTASY VIII", "last_modified": 1580830590, "price_change_number": 9550283}, {"appid": 39210, "name": "FINAL FANTASY XIV Online", "last_modified": 1600280372, "price_change_number": 6949881}, {"appid": 230330, "name": "FINAL FANTASY® XI: Ultimate Collection Seekers Edition", "last_modified": 1531736067, "price_change_number": 9331095}, {"appid": 230350, "name": "FINAL FANTASY®  XI: Ultimate Collection Seekers Edition", "last_modified": 1549382177, "price_change_number": 9331095}, {"appid": 239120, "name": "FINAL FANTASY III", "last_modified": 1585304945, "price_change_number": 9550283}, {"appid": 292120, "name": "FINAL FANTASY® XIII", "last_modified": 1580830392, "price_change_number": 9550283}, {"appid": 292140, "name": "FINAL FANTASY® XIII-2", "last_modified": 1573466446, "price_change_number": 9550283}, {"appid": 312750, "name": "FINAL FANTASY IV", "last_modified": 1578415275, "price_change_number": 9550283}, {"appid": 340170, "name": "FINAL FANTASY TYPE-0™ HD", "last_modified": 1580830865, "price_change_number": 9550283}, {"appid": 345350, "name": "LIGHTNING RETURNS™: FINAL FANTASY® XIII", "last_modified": 1572250705, "price_change_number": 9550283}, {"appid": 346830, "name": "FINAL FANTASY IV: THE AFTER YEARS", "last_modified": 1547739205, "price_change_number": 9550283}, {"appid": 359870, "name": "FINAL FANTASY X/X-2 HD Remaster", "last_modified": 1567793822, "price_change_number": 9550283}, {"appid": 377840, "name": "FINAL FANTASY IX", "last_modified": 1596707344, "price_change_number": 9550283}, {"appid": 382890, "name": "FINAL FANTASY V", "last_modified": 1578415532, "price_change_number": 9550283}, {"appid": 382900, "name": "FINAL FANTASY VI", "last_modified": 1578415704, "price_change_number": 9550283}, {"appid": 552700, "name": "WORLD OF FINAL FANTASY®", "last_modified": 1580830916, "price_change_number": 9550283}, {"appid": 595520, "name": "FINAL FANTASY XII THE ZODIAC AGE", "last_modified": 1588783777, "price_change_number": 9550283}, {"appid": 637650, "name": "FINAL FANTASY XV WINDOWS EDITION", "last_modified": 1592962568, "price_change_number": 9550283}, {"appid": 648600, "name": "FINAL FANTASY XV WINDOWS EDITION MOD ORGANIZER", "last_modified": 1538145960, "price_change_number": 0}, {"appid": 921590, "name": "DISSIDIA FINAL FANTASY NT Free Edition", "last_modified": 1583341768, "price_change_number": 9550283}, {"appid": 1026680, "name": "FINAL FANTASY VIII - REMASTERED", "last_modified": 1582570782, "price_change_number": 9550283}]}
* none
    - action_game_search
    - slot{"game_index": 10}
* game_appid{"game_appid": 239120}
    - slot{"game_appid": 239120}
    - action_game_price
    - slot{"game": null}
    - slot{"game_index": 0}
    - slot{"found_games": null}
    - slot{"game_appid": null}
    - utter_anything_else

## game_price to game_players ask_game_name 1 resultado
* game_price
    - utter_ask_game_name
* game_players{"game": "Dota 2"}
    - slot{"game": "Dota 2"}
    - action_reset_entities
    - slot{"game": null}
    - slot{"game_index": 0}
    - slot{"found_games": null}
    - slot{"game_appid": null}
    - action_game_search
    - slot{"game_appid": 570}
    - action_game_players
    - slot{"game": null}
    - slot{"game_index": 0}
    - slot{"found_games": null}
    - slot{"game_appid": null}
    - utter_anything_else

## game_price to game_players ask_game_name varios resultados none então escolha
* game_price
    - utter_ask_game_name
* game_players{"game": "Final fantasy"}
    - slot{"game": "Final fantasy"}
    - action_reset_entities
    - slot{"game": null}
    - slot{"game_index": 0}
    - slot{"found_games": null}
    - slot{"game_appid": null}
    - action_game_search
    - slot{"game": "Final fantasy"}
    - slot{"game_index": 5}
    - slot{"found_games": [{"appid": 39140, "name": "FINAL FANTASY VII", "last_modified": 1574952550, "price_change_number": 9550283}, {"appid": 39150, "name": "FINAL FANTASY VIII", "last_modified": 1580830590, "price_change_number": 9550283}, {"appid": 39210, "name": "FINAL FANTASY XIV Online", "last_modified": 1600280372, "price_change_number": 6949881}, {"appid": 230330, "name": "FINAL FANTASY® XI: Ultimate Collection Seekers Edition", "last_modified": 1531736067, "price_change_number": 9331095}, {"appid": 230350, "name": "FINAL FANTASY®  XI: Ultimate Collection Seekers Edition", "last_modified": 1549382177, "price_change_number": 9331095}, {"appid": 239120, "name": "FINAL FANTASY III", "last_modified": 1585304945, "price_change_number": 9550283}, {"appid": 292120, "name": "FINAL FANTASY® XIII", "last_modified": 1580830392, "price_change_number": 9550283}, {"appid": 292140, "name": "FINAL FANTASY® XIII-2", "last_modified": 1573466446, "price_change_number": 9550283}, {"appid": 312750, "name": "FINAL FANTASY IV", "last_modified": 1578415275, "price_change_number": 9550283}, {"appid": 340170, "name": "FINAL FANTASY TYPE-0™ HD", "last_modified": 1580830865, "price_change_number": 9550283}, {"appid": 345350, "name": "LIGHTNING RETURNS™: FINAL FANTASY® XIII", "last_modified": 1572250705, "price_change_number": 9550283}, {"appid": 346830, "name": "FINAL FANTASY IV: THE AFTER YEARS", "last_modified": 1547739205, "price_change_number": 9550283}, {"appid": 359870, "name": "FINAL FANTASY X/X-2 HD Remaster", "last_modified": 1567793822, "price_change_number": 9550283}, {"appid": 377840, "name": "FINAL FANTASY IX", "last_modified": 1596707344, "price_change_number": 9550283}, {"appid": 382890, "name": "FINAL FANTASY V", "last_modified": 1578415532, "price_change_number": 9550283}, {"appid": 382900, "name": "FINAL FANTASY VI", "last_modified": 1578415704, "price_change_number": 9550283}, {"appid": 552700, "name": "WORLD OF FINAL FANTASY®", "last_modified": 1580830916, "price_change_number": 9550283}, {"appid": 595520, "name": "FINAL FANTASY XII THE ZODIAC AGE", "last_modified": 1588783777, "price_change_number": 9550283}, {"appid": 637650, "name": "FINAL FANTASY XV WINDOWS EDITION", "last_modified": 1592962568, "price_change_number": 9550283}, {"appid": 648600, "name": "FINAL FANTASY XV WINDOWS EDITION MOD ORGANIZER", "last_modified": 1538145960, "price_change_number": 0}, {"appid": 921590, "name": "DISSIDIA FINAL FANTASY NT Free Edition", "last_modified": 1583341768, "price_change_number": 9550283}, {"appid": 1026680, "name": "FINAL FANTASY VIII - REMASTERED", "last_modified": 1582570782, "price_change_number": 9550283}]}
* none
    - action_game_search
    - slot{"game_index": 10}
* game_appid{"game_appid": 239120}
    - slot{"game_appid": 239120}
    - action_game_players
    - slot{"game": null}
    - slot{"game_index": 0}
    - slot{"found_games": null}
    - slot{"game_appid": null}
    - utter_anything_else

## game_players to game_price ask_game_name 1 resultado
* game_players
    - utter_ask_game_name
* game_price{"game": "Dota 2"}
    - slot{"game": "Dota 2"}
    - action_reset_entities
    - slot{"game": null}
    - slot{"game_index": 0}
    - slot{"found_games": null}
    - slot{"game_appid": null}
    - action_game_search
    - slot{"game_appid": 570}
    - action_game_price
    - slot{"game": null}
    - slot{"game_index": 0}
    - slot{"found_games": null}
    - slot{"game_appid": null}
    - utter_anything_else

## game_players to game_price ask_game_name varios resultados none então escolha
* game_players
    - utter_ask_game_name
* game_price{"game": "Final fantasy"}
    - slot{"game": "Final fantasy"}
    - action_reset_entities
    - slot{"game": null}
    - slot{"game_index": 0}
    - slot{"found_games": null}
    - slot{"game_appid": null}
    - action_game_search
    - slot{"game": "Final fantasy"}
    - slot{"game_index": 5}
    - slot{"found_games": [{"appid": 39140, "name": "FINAL FANTASY VII", "last_modified": 1574952550, "price_change_number": 9550283}, {"appid": 39150, "name": "FINAL FANTASY VIII", "last_modified": 1580830590, "price_change_number": 9550283}, {"appid": 39210, "name": "FINAL FANTASY XIV Online", "last_modified": 1600280372, "price_change_number": 6949881}, {"appid": 230330, "name": "FINAL FANTASY® XI: Ultimate Collection Seekers Edition", "last_modified": 1531736067, "price_change_number": 9331095}, {"appid": 230350, "name": "FINAL FANTASY®  XI: Ultimate Collection Seekers Edition", "last_modified": 1549382177, "price_change_number": 9331095}, {"appid": 239120, "name": "FINAL FANTASY III", "last_modified": 1585304945, "price_change_number": 9550283}, {"appid": 292120, "name": "FINAL FANTASY® XIII", "last_modified": 1580830392, "price_change_number": 9550283}, {"appid": 292140, "name": "FINAL FANTASY® XIII-2", "last_modified": 1573466446, "price_change_number": 9550283}, {"appid": 312750, "name": "FINAL FANTASY IV", "last_modified": 1578415275, "price_change_number": 9550283}, {"appid": 340170, "name": "FINAL FANTASY TYPE-0™ HD", "last_modified": 1580830865, "price_change_number": 9550283}, {"appid": 345350, "name": "LIGHTNING RETURNS™: FINAL FANTASY® XIII", "last_modified": 1572250705, "price_change_number": 9550283}, {"appid": 346830, "name": "FINAL FANTASY IV: THE AFTER YEARS", "last_modified": 1547739205, "price_change_number": 9550283}, {"appid": 359870, "name": "FINAL FANTASY X/X-2 HD Remaster", "last_modified": 1567793822, "price_change_number": 9550283}, {"appid": 377840, "name": "FINAL FANTASY IX", "last_modified": 1596707344, "price_change_number": 9550283}, {"appid": 382890, "name": "FINAL FANTASY V", "last_modified": 1578415532, "price_change_number": 9550283}, {"appid": 382900, "name": "FINAL FANTASY VI", "last_modified": 1578415704, "price_change_number": 9550283}, {"appid": 552700, "name": "WORLD OF FINAL FANTASY®", "last_modified": 1580830916, "price_change_number": 9550283}, {"appid": 595520, "name": "FINAL FANTASY XII THE ZODIAC AGE", "last_modified": 1588783777, "price_change_number": 9550283}, {"appid": 637650, "name": "FINAL FANTASY XV WINDOWS EDITION", "last_modified": 1592962568, "price_change_number": 9550283}, {"appid": 648600, "name": "FINAL FANTASY XV WINDOWS EDITION MOD ORGANIZER", "last_modified": 1538145960, "price_change_number": 0}, {"appid": 921590, "name": "DISSIDIA FINAL FANTASY NT Free Edition", "last_modified": 1583341768, "price_change_number": 9550283}, {"appid": 1026680, "name": "FINAL FANTASY VIII - REMASTERED", "last_modified": 1582570782, "price_change_number": 9550283}]}
* none
    - action_game_search
    - slot{"game_index": 10}
* game_appid{"game_appid": 239120}
    - slot{"game_appid": 239120}
    - action_game_price
    - slot{"game": null}
    - slot{"game_index": 0}
    - slot{"found_games": null}
    - slot{"game_appid": null}
    - utter_anything_else