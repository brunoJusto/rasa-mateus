import json
import re
import dotenv
import os
import steam.webapi
import urllib3

urllib3.disable_warnings()
dotenv.load_dotenv()

STEAM_API_KEY = os.getenv(key='STEAM_API_KEY')
api = steam.webapi.WebAPI(STEAM_API_KEY)
http = urllib3.PoolManager()


def regex(game) -> str:
	game = re.sub(r'[\-]', ' ', game)
	game = re.sub(r'[^A-Za-z0-9 ]', '', game).strip()
	game = re.sub(r' {2,}', ' ', game)
	return game


def get_games_list():
	try:
		jogos = api.IStoreService.GetAppList(
			include_dlc=False,
			include_software=False,
			include_videos=False,
			max_results=50000
		)

		return jogos['response']['apps']
	except:
		return None


def quantia_de_jogadores(appid: int):
	try:
		return api.ISteamUserStats.GetNumberOfCurrentPlayers(appid=appid)['response']['player_count']
	except:
		return 0


def game_details(appid):
	appid = str(appid)
	url = f"https://store.steampowered.com/api/appdetails/?appids={appid}"
	req = http.request('GET', url)
	result = json.loads(req.data)
	if result[appid]['success']:
		return result[appid]['data']
	return None


def busca_jogo(nome):
	jogos = get_games_list()
	return [jogo for jogo in jogos if regex(nome.lower()) in regex(jogo['name'].lower())]
