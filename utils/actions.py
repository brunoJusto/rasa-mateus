from rasa_sdk.events import SlotSet

from utils import steam_util


def reset_entities():
	return [
		SlotSet("game", None),
		SlotSet('game_index', 0),
		SlotSet('found_games', None),
		SlotSet('game_appid', None)
	]


def game_search(game_name):
	games_list = steam_util.busca_jogo(game_name)
	games_list = sorted(games_list, key=lambda k: k['name'])

	if len(games_list):
		return games_list
	else:
		return None


def help_buttons():
	buttons = [
		{'title': 'Preço de um jogo', 'payload': '/game_price'},
		{'title': 'Quantia de jogadores', 'payload': '/game_players'}
	]

	return buttons


def linebreak(tracker):
	if tracker.get_latest_input_channel() == 'socketio':
		return '  \n'
	else:
		return '\n'