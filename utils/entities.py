import utils.steam_util as steam_utils


def game():
    with open('../data/entities/game.md', 'w+', encoding='UTF-8') as f:
        games = set([steam_utils.regex(game['name']) for game in steam_utils.get_games_list()])
        games.remove("")
        f.write("## intent:game\n")
        for game in sorted(games):
            f.write(f'- [{game}](game)\n')


def initialize_all():
    game()

if __name__ == '__main__':
    initialize_all()
