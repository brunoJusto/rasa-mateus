# README #


## Para que serve? ##

Chatbot para verificar dados do Steam

## Como utilizo? ###

#### Geração da chave de acesso à API ####

 - Obtenha a chave acessando este [link](https://steamcommunity.com/dev/apikey)
 - Crie um arquivo .env
 - Adicione `STEAM_API_KEY=<sua chave>` no arquivo
 
 Este arquivo deve estar na raíz do diretorio

#### Resolvendo dependências: ####

Para resolver dependências utilize:

```
pip install -r requirements.txt
```

#### Gerando arquivo com o nome dos jogos
 - Executar o script localizado em `utils\entities.py`
 
 Ao finalizar a execução do script deve ser gerado um arquivo `game.md` na pasta `data\entities`

#### Treinando o bot
 
 Para treinar o bot basta executar o comando abaixo:
 
```
  rasa train
```

#### Executando o bot

- Para iniciar suas ações:
```
rasa run actions
```
##### iniciar o serviço do Rasa:
- Apenas no terminal: ```rasa shell```
- Utilizando Rasa X :```rasa X```
- Sem Rasa X: ```rasa run```

Caso for fazer integrações utilize um dos 2 últimos (recomendo Rasa X pois salva o histórico de conversas para visualizar posteriormente).




Opcional:

Para utilizar o WebChat é bem simples:
- Inicie o serviço do Rasa
- Rodar o arquivo `app.py` localizado na pasta `web`

## Organização do Projeto

```
    actions/            // Arquivos python com ações customizadas
    data/
        entities/       // Entidades
        - nlu.md        // Intenções e entidades          
        - stories.md    // Histórias e fluxos de conversa
    utils/              // Funções Úteis e constantes
    web/                // Exemplo de WebChat utilizando o Rasa
    config.yml          // Configurações do assistente
    credentials.yml     // Credenciais de acesso às plataformas de chat e voice
    domain.yml          // Arquivo que instancia as intenções, entidades e ações do assistente
    endpoints.yml       // Endpoints que o bot pode utilizar
```