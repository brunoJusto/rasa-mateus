from typing import Text, Any, Dict, List
from rasa_sdk import Action, Tracker
from rasa_sdk.executor import CollectingDispatcher

import utils


class ActionGamePrice(Action):

	def name(self) -> Text:
		return "action_game_price"

	def run(self, dispatcher: CollectingDispatcher, tracker: Tracker, domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:
		lb = utils.actions.linebreak(tracker)

		game = utils.steam_util.game_details(tracker.get_slot('game_appid'))
		try:
			text = f"{game['name']}{lb}"

			if game['is_free']:  # é gratuito
				text += f'Preço: Gratuito{lb}'

			elif game['price_overview']['discount_percent'] > 0:  # ta com desconto
				text += f'Preço Original: {game["price_overview"]["initial_formatted"]}{lb}'
				text += f'Preço Atual: {game["price_overview"]["final_formatted"]}{lb}'
				text += f'Desconto de {game["price_overview"]["discount_percent"]}%{lb}'

			else:  # não ta com desconto
				text += f"Preço atual: {game['price_overview']['final_formatted']}{lb}"
				text += f"Não está em promoção{lb}"

		except KeyError as e:
			if e.args[0] == 'price_overview':
				text += 'Não há valor registrado.'

		dispatcher.utter_message(
			text=text,
			image=game['header_image']
		)
		return utils.actions.reset_entities()
