from typing import Text, Dict, Any, List
from rasa_sdk import Action, Tracker
from rasa_sdk.executor import CollectingDispatcher

import utils


class ActionGamePlayers(Action):

	def name(self) -> Text:
		return "action_game_players"

	def run(self, dispatcher: CollectingDispatcher, tracker: Tracker, domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:
		lb = utils.actions.linebreak(tracker)
		appid = tracker.get_slot('game_appid')

		game = utils.steam_util.game_details(appid)
		current_players = utils.steam_util.quantia_de_jogadores(appid)
		dispatcher.utter_message(
			text = f'{game["name"]}{lb}{current_players} pessoas jogando atualmente',
			image=game['header_image']
		)
		return utils.actions.reset_entities()