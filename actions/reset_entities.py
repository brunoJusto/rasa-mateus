from typing import Any, Text, Dict, List

from rasa_sdk import Action, Tracker
from rasa_sdk.executor import CollectingDispatcher

import utils


class ActionResetEntities(Action):

	def name(self) -> Text:
		return "action_reset_entities"

	def run(self, dispatcher: CollectingDispatcher, tracker: Tracker, domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:
		return utils.actions.reset_entities()
