from typing import Text, Dict, Any, List

from rasa_sdk import Action, Tracker
from rasa_sdk.executor import CollectingDispatcher

import utils


class ActionHelp(Action):

	def name(self) -> Text:
		return "action_help"

	def run(self, dispatcher: CollectingDispatcher, tracker: Tracker, domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:
		lb = utils.actions.linebreak(tracker)

		frase = f"No momento eu sou capaz de informar:{lb}" \
			f" - O preço atual de um jogo{lb}" \
			f" - Quantas pessoas estão jogando determinado jogo{lb}" \
			"O que gostaria de fazer?"

		dispatcher.utter_message(
			text=frase,
			buttons=utils.actions.help_buttons(),
		)
		return []
