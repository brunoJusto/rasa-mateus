import random
from typing import Text, Any, Dict, List

from rasa_sdk import Action, Tracker
from rasa_sdk.executor import CollectingDispatcher


class ActionGreet(Action):

	def name(self) -> Text:
		return "action_greet"

	def run(self, dispatcher: CollectingDispatcher, tracker: Tracker, domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:
		frases = ['Olá', 'Oi']
		actions = [action['parse_data'] for action in tracker.events if action['event'] == 'user']
		actions = [action for action in actions if action['intent']['name'] == 'greet']
		if len(actions) <= 1:
			frase = f'{random.choice(frases)}!'
		elif len(actions) == 2:
			frase = f'{random.choice(frases)}...?'
		else:
			frase = '...'
		dispatcher.utter_message(frase)
		return []
