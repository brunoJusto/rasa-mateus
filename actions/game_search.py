from typing import Any, Text, Dict, List

from rasa_sdk import Action, Tracker
from rasa_sdk.events import SlotSet
from rasa_sdk.executor import CollectingDispatcher

from utils import steam_util,actions


class ActionGameSearch(Action):

	def name(self) -> Text:
		return "action_game_search"

	def run(self, dispatcher: CollectingDispatcher, tracker: Tracker, domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:
		try:
			slot_updates = []
			game_name = tracker.get_slot('game')
			game_index = tracker.get_slot('game_index')
			found_games = tracker.get_slot('found_games')

			if game_name is None:
				entities = tracker.latest_message['entities']
				for entity in entities:
					if entity['entity'] == 'game':
						game_name = entity['value']
						slot_updates.append(SlotSet("game", game_name))
						break

			if found_games is None:
				games = steam_util.busca_jogo(game_name)
				if games != found_games:
					slot_updates.extend([SlotSet('game_index', game_index + 5), SlotSet('found_games', games)])
				else:
					slot_updates.append(SlotSet('game_index', game_index + 5))
			else:
				games = found_games
				slot_updates.append(SlotSet('game_index', game_index + 5))

			buttons = []
			if games is not None:
				if len(games) > 1:
					if games[game_index:game_index + 5]:
						for game in games[game_index:game_index+5]:
							buttons.append(
								{
									'title': game['name'],
									'payload': f'/game_appid{{"game_appid":{game["appid"]}}}'
								}
							)
						buttons.append({'title': 'Nenhum dos anteriores', 'payload': '/none'})

						dispatcher.utter_message(
							text='Encontrei alguns resultados:',
							buttons=buttons,
							button_type='vertical'
						)
					else:
						dispatcher.utter_message('Não há mais resultados para a busca')
						slot_updates = actions.reset_entities()

				elif len(games) == 1:
					slot_updates = [SlotSet("game_appid", games[0]['appid'])]


				else:
					dispatcher.utter_message('Desculpe, não consegui encontrar o jogo')
					slot_updates = actions.reset_entities()

			else:
				dispatcher.utter_message('Desculpe, não consegui encontrar o jogo')
				slot_updates = actions.reset_entities()

		except Exception as e:
			dispatcher.utter_message("Um erro ocorreu")
			slot_updates = actions.reset_entities()

		return slot_updates
