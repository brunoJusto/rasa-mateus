import logging
logging.basicConfig(
	filename='Error.log',
	format='%(asctime)s,%(msecs)d %(name)s %(levelname)s %(message)s',
	datefmt='%H:%M:%S',
	level=logging.ERROR
)
